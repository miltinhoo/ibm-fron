export interface ConsumoModel {
    id: number;
    fecha: Date;
    descripcion: string;
    monto: number;
    numeroTarjeta: number;
}
