export interface TarjetaModel {
    numeroTarjeta: string;
    ccv: number;
    tipo: string;
    idCliente: number;
}
