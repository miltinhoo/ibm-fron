export interface AsesorModel {
    id?: number;
    nombre: string;
    especialidad: string;
}
