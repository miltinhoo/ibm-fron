export interface ClienteModel {
    idCliente?: number;
    nombre: string;
    direccion: string;
    ciudad: string;
    telefono: number;
}
