import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { APP_ROUTING } from './app.routes';
import { ClienteComponent } from './components/cliente/cliente.component';
import { AsesorComponent } from './components/asesor/asesor.component';
import { ConsumoComponent } from './components/consumo/consumo.component';
import { AsesorService } from './service/asesor.service';
import { ClienteService } from './service/cliente.service';
import { ConsumoService } from './service/consumo.service';
import { TarjetaService } from './service/tarjeta.service';
import { HttpClientModule } from '@angular/common/http';
import { ClienteTablaComponent } from './components/cliente/cliente-tabla/cliente-tabla.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TarjetaComponent } from './components/tarjeta/tarjeta.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ClienteComponent,
    AsesorComponent,
    ConsumoComponent,
    ClienteTablaComponent,
    TarjetaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    APP_ROUTING,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AsesorService,
    ClienteService,
    ConsumoService,
    TarjetaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
