import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { ClienteComponent } from "./components/cliente/cliente.component";
import { AsesorComponent } from "./components/asesor/asesor.component";
import { ConsumoComponent } from "./components/consumo/consumo.component";
import { ClienteTablaComponent } from "./components/cliente/cliente-tabla/cliente-tabla.component";

const APP_ROUTES: Routes = [
    { path: 'clientes', component: ClienteTablaComponent},
    { path: 'cliente/:idCliente', component: ClienteComponent},
    { path: 'asesores', component: AsesorComponent},
    { path: 'consumos/:numeroTarjeta', component: ConsumoComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'clientes'}
]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
