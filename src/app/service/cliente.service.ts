import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ClienteModel } from '../model/cliente.model';
import { throwError, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NgForm } from '@angular/forms'

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private pathConsulta = `http://localhost:8080/cliente`;
  private pathCliente = `http://localhost:8080/cliente/:idCliente`;

  constructor(
    private http: HttpClient
  ) { }

  consultarClientes(): Observable<any[]> {
    return this.http.get<any[]>(this.pathConsulta)
      .pipe(
        map((clientes: any[]) => clientes),
        catchError(this.handleError)
      );
  }

  consultarClientePorId(idCliente: string): Observable<any> {
    return this.http.get<any>(this.pathCliente.replace(':idCliente', idCliente))
      .pipe(
        map((clientes: any) => clientes),
        catchError(this.handleError)
      );
  }

  actualizarCliente(cliente: ClienteModel): Observable<any> {
    const params = JSON.stringify(cliente);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<any>(this.pathConsulta, params, { headers: headers })
      .pipe(
        map((clientes: ClienteModel[]) => clientes),
        catchError(this.handleError)
      );
  }

  guardarNuevoCliente(cliente: ClienteModel): Observable<any> {
    const params = JSON.stringify(cliente);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.patch<any>(this.pathConsulta, params, { headers: headers })
      .pipe(
        map((clientes: ClienteModel[]) => clientes),
        catchError(this.handleError)
      );
  }

  eliminarCliente(idCliente: string): Observable<ClienteModel[]> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.delete<ClienteModel[]>(this.pathCliente.replace(':idCliente', idCliente), { headers: headers })
      .pipe(
        map((clientes: ClienteModel[]) => clientes),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
