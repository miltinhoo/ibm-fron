import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { TarjetaModel } from '../model/tarjeta.model';

@Injectable({
  providedIn: 'root'
})
export class TarjetaService {
  private pathTarjeta = `http://localhost:8080/tarjeta/:idCliente`;
  constructor(
    private http: HttpClient
  ) { }

  consultarTarjetaPorIdCliente(idCliente: string): Observable<TarjetaModel[]> {
    return this.http.get<TarjetaModel[]>(this.pathTarjeta.replace(':idCliente', idCliente))
      .pipe(
        map((tarjetas: TarjetaModel[]) => tarjetas),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
