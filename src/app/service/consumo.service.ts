import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ConsumoModel } from '../model/consumo.model';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConsumoService {
  private pathConsumoPorTarjeta = `http://localhost:8080/consumo/:numeroTarjeta/tarjeta`;
  constructor(
    private http: HttpClient
  ) { }

  consultarConsumoPorNumeroTarjeta(numeroTarjeta: string): Observable<ConsumoModel[]> {
    return this.http.get<ConsumoModel[]>(this.pathConsumoPorTarjeta.replace(':numeroTarjeta', numeroTarjeta))
      .pipe(
        map((consumos: ConsumoModel[]) => consumos),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
