import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AsesorModel } from '../model/asesor.model';

@Injectable({
  providedIn: 'root'
})
export class AsesorService {
  private pathConsulta = `http://localhost:8080/asesor`;
  private pathDeleteAsesor = `http://localhost:8080/asesor/:idAsesor`;
  constructor(private http: HttpClient) { }

  consultarAsesores(): Observable<AsesorModel[]> {
    return this.http.get<AsesorModel[]>(this.pathConsulta)
      .pipe(
        map((asesores: AsesorModel[]) => asesores),
        catchError(this.handleError)
      );
  }

  guardarAsesor(asesor: AsesorModel): Observable<AsesorModel[]> {
    const params = JSON.stringify(asesor);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<AsesorModel[]>(this.pathConsulta, params, { headers: headers })
      .pipe(
        map((asesores: AsesorModel[]) => asesores),
        catchError(this.handleError)
      );
  }

  eliminarAsesor(idAsesor: string): Observable<AsesorModel[]> {
    const params = JSON.stringify(idAsesor);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.delete<AsesorModel[]>(this.pathDeleteAsesor.replace(':idAsesor', idAsesor), { headers: headers })
      .pipe(
        map((asesores: AsesorModel[]) => asesores),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
