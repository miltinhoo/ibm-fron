import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConsumoModel } from 'src/app/model/consumo.model';
import { ConsumoService } from 'src/app/service/consumo.service';
import { tap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-consumo',
  templateUrl: './consumo.component.html',
  styleUrls: ['./consumo.component.scss']
})
export class ConsumoComponent implements OnInit {
  consumos: ConsumoModel[];
  constructor(
    private activatedRout: ActivatedRoute,
    private consumoService: ConsumoService
  ) { }

  ngOnInit() {
    this.activatedRout.params.subscribe(params => 
      this.consultarConsumoPorTarjeta(params['numeroTarjeta'])
    );
  }

  consultarConsumoPorTarjeta(numeroTarjeta: string) {
    this.consumoService.consultarConsumoPorNumeroTarjeta(numeroTarjeta)
      .pipe(
        tap((consumos: ConsumoModel[]) => this.consumos = consumos),
        catchError(error => {
          console.log(error);
          return error;
        })
      )
      .subscribe(_ => _);
  }

}
