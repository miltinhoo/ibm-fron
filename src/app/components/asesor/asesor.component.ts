import { Component, OnInit } from '@angular/core';
import { AsesorModel } from 'src/app/model/asesor.model';
import { AsesorService } from 'src/app/service/asesor.service';
import { map, catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'app-asesor',
  templateUrl: './asesor.component.html',
  styleUrls: ['./asesor.component.scss']
})
export class AsesorComponent implements OnInit {
  asesores: AsesorModel[];
  nuevoAsesor: AsesorModel;
  constructor(
    private asesorService: AsesorService
  ) { }

  ngOnInit() {
    this.initNuevoAsesor();
    this.consultarAsesores();
  }

  initNuevoAsesor() {
    this.nuevoAsesor = {
      nombre: '',
      especialidad: ''
    }
  }

  consultarAsesores() {
    this.asesorService.consultarAsesores()
      .pipe(
        map((asesores: AsesorModel[]) => {
          this.asesores = asesores;
          this.initNuevoAsesor();
        }),
        catchError(error => {
          console.log(error);
          return error;
        })
      )
      .subscribe(_ => _);
  }

  guardarAsesor(asesor: AsesorModel = this.nuevoAsesor) {
    this.asesorService.guardarAsesor(asesor)
      .pipe(
        tap((asesores: AsesorModel[]) => {
          this.asesores = asesores;
          this.initNuevoAsesor();
        }
        ),
        catchError(error => {
          console.log(error);
          return error;
        })
      )
      .subscribe(_ => _);
  }

  eliminarAsesor(idAsesor: string) {
    this.asesorService.eliminarAsesor(idAsesor)
      .pipe(
        tap((asesores: AsesorModel[]) => {
          this.asesores = asesores;
          this.initNuevoAsesor();
        }),
        catchError(error => {
          console.log(error);
          return error;
        })
      )
      .subscribe(_ => _);
  }

}
