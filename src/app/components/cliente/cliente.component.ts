import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../../service/cliente.service';
import { ClienteModel } from '../../model/cliente.model';
import { tap, catchError } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {
  cliente: ClienteModel;
  constructor(
    private clienteService: ClienteService,
    private activatedRout: ActivatedRoute
  ) {
    this.activatedRout.params.subscribe(params => {
      this.loadCliente( params['idCliente'] )
    });
   }

  ngOnInit() {
    
  }

  loadCliente(idCliente: string) {
    this.clienteService.consultarClientePorId(idCliente)
    .pipe(
      tap((cliente: ClienteModel) => this.cliente = cliente),
      catchError(error => error)
    )
      .subscribe(_ => _);
  }

  guardarCliente() {
    this.clienteService.actualizarCliente(this.cliente)
    .pipe(
      tap(clientes => console.log(`cliente guardado con exito`)
      ),
      catchError(error => error)
    )
    .subscribe(_ => _);
  }

}
