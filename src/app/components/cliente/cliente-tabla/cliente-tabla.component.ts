import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/service/cliente.service';
import { Router } from '@angular/router';
import { tap, catchError } from 'rxjs/operators';
import { ClienteModel } from 'src/app/model/cliente.model';

@Component({
  selector: 'app-cliente-tabla',
  templateUrl: './cliente-tabla.component.html',
  styleUrls: ['./cliente-tabla.component.scss']
})
export class ClienteTablaComponent implements OnInit {
  clienteNuevo: ClienteModel;
  clientes: ClienteModel[] = [];
  constructor(
    private clienteService: ClienteService,
    private router: Router
  ) {
    this.loadInit();
  }

  ngOnInit() {
    this.loadClientes();
  }

  loadInit() {
    this.clienteNuevo = {
      nombre: '',
      direccion: '',
      ciudad: '',
      telefono: undefined
    }
  }

  loadClientes() {
    this.clienteService.consultarClientes()
      .pipe(
        tap(
          (clientes: ClienteModel[]) => {
            this.clientes = clientes;
            this.loadInit();
          }
        )
      ).subscribe(_ => _);
  }

  verCLiente(idCliente: number) {
    this.router.navigate(['/cliente', idCliente]);
  }

  eliminarCliente(idCliente: number) {
    this.clienteService.eliminarCliente(idCliente.toString())
      .pipe(
        tap((clientes: ClienteModel[]) => {
          this.clientes = clientes;
          this.loadInit();
        }),
        catchError(error => {
          console.log(error);
          return error;
        })
      )
      .subscribe(_ => _);
  }

  guardarNuevoCliente() {
    this.clienteService.guardarNuevoCliente(this.clienteNuevo)
      .pipe(
        tap((clientes: ClienteModel[]) => {
          this.clientes = clientes;
          this.loadInit();
        }),
        catchError(error => {
          console.log(error);
          return error;
        })
      )
      .subscribe(_ => _);
  }

}
