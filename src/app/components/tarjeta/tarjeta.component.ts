import { Component, OnInit, Input } from '@angular/core';
import { TarjetaService } from 'src/app/service/tarjeta.service';
import { tap, catchError } from 'rxjs/operators';
import { TarjetaModel } from 'src/app/model/tarjeta.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjeta',
  templateUrl: './tarjeta.component.html',
  styleUrls: ['./tarjeta.component.scss']
})
export class TarjetaComponent implements OnInit {
  @Input() idCliente: string;
  tarjetas: TarjetaModel[];
  constructor(
    private tarjetaService: TarjetaService,
    private router: Router
  ) { }

  ngOnInit() {
    this.consultarTarjetas();
  }

  consultarTarjetas() {
    this.tarjetaService.consultarTarjetaPorIdCliente(this.idCliente)
    .pipe(
      tap((tarjetas: TarjetaModel[]) => this.tarjetas = tarjetas),
      catchError(error => {
        console.log(error);
        return error;
      })
    )
    .subscribe(_ => _);
  }
  
  verConsumo(numeroTarjeta: string) {
    this.router.navigate( ['/consumos', numeroTarjeta] );
  }
}
